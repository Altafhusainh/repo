﻿using EmployeeData.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeData.Controllers
{
    public class EmployeeController : Controller
    {
        public ActionResult Index()
        {
            Employee e = new Employee();
            e.empid = 101;
            e.empname = "Altaf";
            e.location = "Banaglu";
            e.designation = "SE";

            ViewBag.EmpID = e.empid;
            ViewBag.EmpName = e.empname;
            ViewBag.Location = e.location;
            ViewBag.Designation = e.designation;
        
            return View();
        }
    }
}
