﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeData.Models
{
    public class Employee
    {
        public int empid { get; set; }
        public string empname { get; set; }
        public string location { get; set; }
        public string designation { get; set; }
    }
}
